<?php

namespace App\Controllers;

use App\Request;
use App\Response;

class HomepageController implements ControllerInterface
{
    public function __invoke(Request $request): Response
    {
        return new Response(json_encode([
            'HOMEPAGE TEST',
            'param1' => 123
        ]), [
            'Content-Type: application/json'
        ]);
    }
}