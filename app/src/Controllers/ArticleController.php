<?php

namespace App\Controllers;

use App\Layout;
use App\Request;
use App\Response;

class ArticleController implements ControllerInterface
{
    public function __invoke(Request $request): Response
    {
        $layout = new Layout($request,"article","article");
        return new Response($layout->renderPage());
    }
}