<?php

namespace App;

use App\Controllers\ArticleController;
use App\Controllers\HomepageController;

class ServiceContainer
{
    private static $instance;

    private $services;

    private function __construct()
    {
        $this->services['router'] = new Router([
            'homepage' => [
                'path' => '/',
                'controller' => new HomepageController()
            ],
            'article' => [
                'path' => '/article/{id}',
                'controller' => new ArticleController()
            ]
        ]);
    }

    /**
     * @return ServiceContainer
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $id
     * @returs object
     * @throws \Exception
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw new \Exception(sprintf('Service "%s" not defined', $id));
        }

        return $this->services[$id];
    }

    /**
     * @param $id
     * @return bool
     */
    public function has($id)
    {
        return isset($this->services[$id]);
    }
}