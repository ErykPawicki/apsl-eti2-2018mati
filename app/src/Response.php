<?php

namespace App;

class Response
{
    /**
     * @var string
     */
    private $body;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var int
     */
    private $status;

    public function __construct($body = '', $headers = [], $status = 200)
    {
        $this->body = $body;
        $this->headers = $headers;
        $this->status = $status;
    }

    public function getHeaders()
    {
        return array_merge([
                sprintf('HTTP/1.1 %s', $this->status),
                sprintf('Content-Length: %d', strlen($this->body))
            ],
            $this->headers
        );
    }

    public function getBody()
    {
        return $this->body;
    }
}