<?php

namespace App;

class Layout
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $page;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param Request $request
     * @param string $page
     * @param string $name
     */
    public function __construct(Request $request, string $page, string $name)
    {
        $this->page = $page;
        $this->name = $name;
        $this->request = $request;
    }

    public function render()
    {
        extract([
            'title' => $this->title,
            'content' => $this->renderPage()
        ]);
        include __DIR__ . "/../layouts/{$this->name}.php";
    }

    public function renderPage()
    {
        ob_start();
        extract([
            'request' => $this->request,
            'router' => ServiceContainer::getInstance()->get('router')
        ]);
        include __DIR__ . "/../templates/{$this->page}.php";

        return ob_get_clean();
    }
}